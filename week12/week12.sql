# VOID, IN, OUT, INOUT 
# IN the result is NOT visible to user
# OUT the result is visible to user
# IN is the default value (VOID one)


call selectAllCustomers();


call getCustomersbyCity("Barcelona");


set @max_salary= 0 ;
call highestSalary(@max_salary);

# If we print once, we only see affected ones. 

select @max_salary;


set  @m_count=0;
call CountGender( @m_count, "M");

set  @f_count=0;
call CountGender( @f_count, "F");

select @m_count, @f_count;
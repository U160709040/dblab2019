 # LIST ALL CUSTOMERS WHO LIVE IN USA  #

CREATE VIEW usa_customers as 
select CustomerID, CustomerName, ContactName
from Customers
where Country="USA";

select * from usa_customers;

select * 
from usa_customers join Orders on usa_customers.CustomerID  =  Orders.CustomerID
where OrderID in ( 
  select OrderID
  from OrderDetails join products_below_avg_price on OrderDetails.ProductID = products_below_avg_price.ProductID

   );
 #-------------------------------------------------------------------------------------------

# LIST ALL PRODUCTS THAT LESS THAN AVERAGE PRICE  #
CREATE OR REPLACE VIEW products_below_avg_price as
select ProductID, ProductName, Price 
from Products
where Price <  (select avg(Price) from Products);
select * from products_below_avg_price;

 #-------------------------------------------------------------------------------------------


 #DELETE THE VIEW

DROP  VIEW usa_customers;
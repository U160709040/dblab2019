select Country, count(CustomerID)
from Customers
group by country;


###IF YOU USE GROUP BY, IT SHOULD EXIST IN THE SELECT LINE. 


select  ShipperName, count(Orders.OrderID) as OrderID
from Shippers join Orders on Shippers.ShipperID = Orders.ShipperID
group by ShipperName
order by  OrderID desc ; 